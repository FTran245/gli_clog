import selenium

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoAlertPresentException

# from bs4 import BeautifulSoup
# from bs4.dammit import EntitySubstitution
import xml.etree.ElementTree as ET
import json
import xlwt
import os

## Return the values of the atttributes from the response .xml
## Return the values of the atttributes from the response .xml
def get_bet(root):
    for result in root.findall("GameResult"):
        bet = result.attrib.get("stake")
        # print(bet)    
    
    return bet

def get_totalWagerWin(root):
    totalWagerWin = 0
    # for result in root.iter("GameWinInfo"):
    #     totalWagerWin = result.get("totalWagerWin")
    totalWagerWin = root.find('.//*[@totalWagerWin]').get("totalWagerWin")
    return totalWagerWin

def get_CASH_BALANCE(root):
    CASH_BALANCE = 0
    for result in root.findall("Balances"):
        CASH_BALANCE = result.find(".//*[@name='CASH_BALANCE']").get("value")
        # BONUS_BALANCE = result.find(".//*[@name='BONUS_BALANCE']").get("value") # just to have
    
    return CASH_BALANCE

def check_game(root):
    isEndGame = False
    for result in root.iter("GameWinInfo"):
        isEndGame = result.get("isEndGame") # just check the boolean
    
    return isEndGame
 

def start_chrome():
    
    d = DesiredCapabilities.CHROME
    d['goog:loggingPrefs'] = { 'browser':'ALL' } # To monitor the google console response

    # Temporary - maybe not
    cwd = os.getcwd()
    chromepath = cwd + r"\\chromedriver.exe"
    driver = webdriver.Chrome(executable_path = chromepath, desired_capabilities = d)
    driver.get("https://www.google.ca/")

    return driver

# Simple class to handle money
class Money():
    # amount = [] # not sure if we need this one or the other one ...pretty sure the other one? we don't this will make a shared variable among all instances of this class
    def __init__(self):
        self.amount = []

    def convert(self):
        self.amount = list(map(float, self.amount)) # cast the entire list to float

    def divide_by_hundred(self):
        self.amount[:] = [x / 100 for x in self.amount] # divide all numbers in list by 10 (this is called list comprehension for some reason)

# Checks if an alert happens during accounting - it messes up the while() lol
class Alert_Present():
    def __init__(self):
        pass

    def __call__(self, driver):
        try:
            alert = driver.switch_to.alert
            alert.text
        except NoAlertPresentException:
            return False

def sift_response(allresponses):
    gameresponse = []
    ii = 0

    # We need to take the response directly before the end game response to get the correct balance of the enire game cycle
    for resp in allresponses:
        if("\\u003CGameResponse type=\\\"EndGame\\\"" in resp):
                gameresponse.append(allresponses[ii-1])
        ii = ii + 1

    return gameresponse

def write_to_excel(sheet, name, data, start_row, col):
    sheet.write(start_row, col, name)
    start_row = start_row + 1

    for value in data:
        sheet.write(start_row, col, value)
        start_row = start_row + 1

def create_excel(filename, bet, totalWagerWin, CASH_BALANCE):
    book = xlwt.Workbook()
    sh1 = book.add_sheet('Sheet 1')
    
    write_to_excel(sh1, "Bet", bet.amount, 1, 1) # (sheet, name, <data as a list>, start row, start col)
    write_to_excel(sh1, "Win", totalWagerWin.amount, 1, 3)
    write_to_excel(sh1, "End Balance", CASH_BALANCE.amount, 1, 5)

    book.save(filename)

def dump_xml_log(xml):
    print("placeholder")