import selenium

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# from bs4 import BeautifulSoup
# from bs4.dammit import EntitySubstitution
import xml.etree.ElementTree as ET
import json
import xlwt
import os

# User defined
from clog_library import * 

if __name__ =='__main__':

    # Start the driver
    driver = start_chrome()

    allresponses = []
    al = Alert_Present()
    try:
        while True:
            al(driver) # Check for a force tool alert, that will cause a problem
            for entry in driver.get_log('browser'):
                # We have a problem - we can't use this conditional to cover all games, they're slightly different
                # if(bool("\\u003CGameResponse type=\\\"Logic\\\"" in entry["message"])):
                # if(("isEndGame=\\\"Y\\\"" in entry["message"]) and "\\u003CGameResponse type=\\\"Logic\\\"" in entry["message"]):
                if("\\u003CGameResponse type=\\\"Logic\\\"" in entry["message"] or "\\u003CGameResponse type=\\\"EndGame\\\"" in  entry["message"]):
                    print(entry["message"] + "\n")
                    allresponses.append(entry["message"])

    except KeyboardInterrupt: # KeyboardInterrupt is raised if you use Ctrl+C
        pass

    # New change: since we're collecting all the reponses that contain 'GameResponse' we need to find the important ones
    # The important ones will be the responses directly before the 'GameResponse = EndGame' - we're going to have to parse that out
    gameresponse = sift_response(allresponses) # returns a list of all the game reponses we need

    # Here we'll need to manipulate that mess of data from above
    xml_response_list = []
    isEndGame = []

    bet = Money()
    totalWagerWin = Money()
    CASH_BALANCE = Money()
    BONUS_BALANCE = Money()
    root = []

    # goes from 0 to ii - 1, for example range(6) goes from 0 -> 5, not 0 -> 6, so I'll use len() to avoid confusion
    for jj in range(len(gameresponse)):
        # Looks like this works, we needed to escape from the \u003C unicode character (it's unicode for < in HTML)
        # This gets rid of all the escape characters
        # Now we have our string mostly in .xml format, except for the very beginning.
        # It goes <some huge link> .. "got game logic response from game:   ..the xml stuff"
        xml = gameresponse[jj].encode().decode('unicode_escape')
        ind = xml.find("GameResponse") # Hard coded for SG
        xml = xml[ind-1:-1] # this is how you strip the last element of a string in Python lol, weird syntax
        xml_response_list.append(xml) # I suppose we don't actually need this but it might be nice to have in case we want to inspect the full response

        # To parse .xml/html format
        root = ET.fromstring(xml)

        # Check if end game is Y
        # isEndGame.append(check_game(root))

        # Get the bet information
        bet.amount.append(get_bet(root))

        # Get the real total win, totalWagerWin
        totalWagerWin.amount.append(get_totalWagerWin(root))

        # Get the end balance
        CASH_BALANCE.amount.append(get_CASH_BALANCE(root))
        # end for loop

    # Now we want to write to excel
    # Let's convert the numbers to floats first
    bet.convert()
    totalWagerWin.convert()
    CASH_BALANCE.convert()

    bet.divide_by_hundred()
    totalWagerWin.divide_by_hundred()
    CASH_BALANCE.divide_by_hundred()
    print(bet.amount)
    print(totalWagerWin.amount)
    print(CASH_BALANCE.amount)
    # print(isEndGame)

    filename = "accounting.xls"
    create_excel(filename, bet, totalWagerWin, CASH_BALANCE)



